class AddMaxplayersToEvents < ActiveRecord::Migration
  def change
    add_column :events, :maxplayers, :integer
  end
end
