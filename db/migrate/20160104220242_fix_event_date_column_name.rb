class FixEventDateColumnName < ActiveRecord::Migration
  def self.up
    rename_column :events, :date, :start_time
  end

  def self.down
    # rename back if you need or do something else or do nothing
  end
end
