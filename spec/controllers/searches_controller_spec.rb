require 'rails_helper'
require 'spec_helper'
require 'factory_girl_rails'

RSpec.describe SearchesController, type: :controller do
	describe "GET #new" do 
    	it "responds successfully with an HTTP 200 status code" do
    		@user = FactoryGirl.create :user
      		sign_in @user
    		get :new
    		expect(response).to be_success
    		expect(response).to have_http_status(200)
    	end

    	it "renders the correct template" do
    		@user = FactoryGirl.create :user
    		sign_in @user
    		get :new
    		expect(response).to render_template("new")
    	end
    end

    describe "POST #create" do
    	it "creates successfully a search" do
    		@user = FactoryGirl.create :user
    		sign_in @user
    		post :create, search: {  keywords: "asdfasdf" }
    		expect{Search.create}.to change{Search.count}.by(1)
    	end
    end

    describe "GET #show" do 
    	it "renders correct template" do
    		@user = FactoryGirl.create :user
    		sign_in @user
    		seach = FactoryGirl.create :search
    		get :show, id: seach.id
    		expect(response).to have_http_status(200)
    	end
    end
end
