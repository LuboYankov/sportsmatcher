require 'rails_helper'
require 'spec_helper'
require 'factory_girl_rails'

RSpec.describe EventsController, type: :controller do
    describe "GET #index" do
        it "responds successfully with an HTTP 200 status code" do
          get :index
          expect(response).to be_success
          expect(response).to have_http_status(200)
        end

        it "renders the index template" do
          get :index
          expect(response).to render_template("index")
        end
    end

    describe "GET #new" do 
        it "responds successfully with an HTTP 200 status code" do
            @user = FactoryGirl.create :user
            sign_in @user
            get :new
            expect(response).to be_success
            expect(response).to have_http_status(200)
        end

        it "renders the correct template" do
            @user = FactoryGirl.create :user
            sign_in @user
            get :new
            expect(response).to render_template("new")
        end
    end

    describe "POST #create" do
        it "creates successfully an event" do
            @user = FactoryGirl.create :user
            sign_in @user
            post :create, :event => { title: "testing", 
                                      description: "testingdescripiton", 
                                      sporttype: "Football", 
                                      maxplayers: 12, 
                                      start_time: Date.current.tomorrow, 
                                      time: Time.now, 
                                      location: "sofia, bulgaria"
                                    }
            expect(assigns[:event]).to_not be_new_record
        end

        it "shows flash notice" do
            @user = FactoryGirl.create :user
            sign_in @user
            post :create, :event => { title: "testing", 
                                      description: "testingdescripiton", 
                                      sporttype: "Football", 
                                      maxplayers: 12, 
                                      start_time: Date.current.tomorrow, 
                                      time: Time.now, 
                                      location: "sofia, bulgaria"
                                    }
            expect(flash[:notice]).to_not be_blank
        end

        it "fails because of short title" do 
            @user = FactoryGirl.create :user
            sign_in @user
            post :create, :event => { title: "test", 
                                      description: "testingdescripiton", 
                                      sporttype: "Football", 
                                      maxplayers: 12, 
                                      start_time: Date.current.tomorrow, 
                                      time: Time.now, 
                                      location: "sofia, bulgaria"
                                    }
            expect(response).to render_template("new")
        end

        it "fails because of short description" do 
            @user = FactoryGirl.create :user
            sign_in @user
            post :create, :event => { title: "testing", 
                                      description: "testdesc", 
                                      sporttype: "Football", 
                                      maxplayers: 12, 
                                      start_time: Date.current.tomorrow, 
                                      time: Time.now, 
                                      location: "sofia, bulgaria"
                                    }
            expect(response).to render_template("new")
        end

        it "fails because of too many players" do 
            @user = FactoryGirl.create :user
            sign_in @user
            post :create, :event => { title: "testing", 
                                      description: "testingdescripiton", 
                                      sporttype: "Football", 
                                      maxplayers: 123, 
                                      start_time: Date.current.tomorrow, 
                                      time: Time.now, 
                                      location: "sofia, bulgaria"
                                    }
            expect(response).to render_template("new")
        end

        it "fails because of short location length" do 
            @user = FactoryGirl.create :user
            sign_in @user
            post :create, :event => { title: "testing", 
                                      description: "testingdescripiton", 
                                      sporttype: "Football", 
                                      maxplayers: 12, 
                                      start_time: Date.current.tomorrow, 
                                      time: Time.now, 
                                      location: "sof"
                                    }
            expect(response).to render_template("new")
        end

        it "fails because of invalid date" do 
            @user = FactoryGirl.create :user
            sign_in @user
            post :create, :event => { title: "testing", 
                                      description: "testingdescripiton", 
                                      sporttype: "Football", 
                                      maxplayers: 12, 
                                      start_time: Date.current.yesterday, 
                                      time: Time.now, 
                                      location: "sofia, bulgaria"
                                    }
            expect(response).to render_template("new")
        end
    end

    describe "GET #calendar" do 
        it "lists all events" do 
            @user = FactoryGirl.create :user
            sign_in @user
            get :calendar
            expect(response).to render_template('calendar')
        end

        it "responds successfully with an HTTP 200 status code" do 
            @user = FactoryGirl.create :user
            sign_in @user
            get :calendar
            expect(response).to be_success
            expect(response).to have_http_status(200)
        end
    end

    describe "GET #show" do 
        it "responds successfully with an HTTP 200 status code" do
            @user = FactoryGirl.create :user
            sign_in @user
            event = FactoryGirl.create :event
            get :show, id: event.id
            expect(response).to have_http_status(200)
        end

        it "renders the correct template" do 
            @user = FactoryGirl.create :user
            sign_in @user
            event = FactoryGirl.create :event
            get :show, id: event.id
            expect(response).to render_template("show")
        end
    end

    describe "DELETE #destroy" do 
        it "should decrement events count" do 
            @user = FactoryGirl.create :user
            sign_in @user
            event = FactoryGirl.create :event
            expect{ delete :destroy, :id => event.id }.to change(Event, :count).by(-1)
        end

        it "shows a flash message" do 
            @user = FactoryGirl.create :user
            sign_in @user
            event = FactoryGirl.create :event
            delete :destroy, :id => event.id
            expect(flash[:notice]).to_not be_blank
        end
    end

    describe "PUT #update" do
        it "should update an event" do 
            @user = FactoryGirl.create :user
            sign_in @user
            event = FactoryGirl.create(:event, title: "asdfgh", 
                                    description: "asdasdfsdfasasd", 
                                    sporttype: "Football", 
                                    maxplayers: 12, 
                                    start_time: Date.current.tomorrow, 
                                    time: "2000-01-01 01:05:00 UTC", 
                                    location: "sofia, bulgaria" )
            put :update, id: event, event: FactoryGirl.attributes_for(:event)
            expect(assigns(:event)).to eq(event) 
        end

        it "shows flash message" do 
            @user = FactoryGirl.create :user
            sign_in @user
            event = FactoryGirl.create(:event, title: "asdfgh", 
                                    description: "asdasdfsdfasasd", 
                                    sporttype: "Football", 
                                    maxplayers: 12, 
                                    start_time: Date.current.tomorrow, 
                                    time: "2000-01-01 01:05:00 UTC", 
                                    location: "sofia, bulgaria" )
            put :update, id: event, event: FactoryGirl.attributes_for(:event)
            expect(flash[:notice]).to_not be_blank   
        end
    end

    describe "GET #statistics" do
      it "responds successfully with an HTTP 200 status code" do
        @user = FactoryGirl.create :user
        sign_in @user
        get :statistics
        expect(response).to have_http_status(200)
      end
      it "renders the correct template" do 
        @user = FactoryGirl.create :user
        sign_in @user
        get :statistics
        expect(response).to render_template('statistics')
      end
    end
end
