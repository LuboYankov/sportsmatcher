require 'rails_helper'
require 'spec_helper'
require 'factory_girl_rails'

RSpec.describe ParticipantsController, type: :controller do
	describe "POST #create" do
    	it "creates successfully a participant" do
    		@user = FactoryGirl.create :user
    		sign_in @user
    		participant_attr = attributes_for(:participant)
		  	event = Event.last || create(:event, user_id: @user.id)
		  	expect {
		    	post :create, participant: participant_attr, event_id: event.id
		  	}.to change(Participant,:count).by(1)
    	end
    end
end
