require 'rails_helper'
require 'spec_helper'
require 'factory_girl_rails'

RSpec.describe UsersController, type: :controller do
	describe "#GET show" do 
		it "show a user profile" do
			@user = FactoryGirl.create :user
    		sign_in @user
    		get :show, id: @user.id
    		expect(response).to have_http_status(200)
		end
	end
end
