require 'rails_helper'
require 'spec_helper'
require 'factory_girl_rails'

RSpec.describe CommentsController, type: :controller do
	describe "POST #create" do
    	it "creates successfully a comment" do
    		@user = FactoryGirl.create :user
    		sign_in @user
    		comment_attr = attributes_for(:comment)
		  	event = Event.last || create(:event, user_id: @user.id)
		  	expect {
		    	post :create, comment: comment_attr, event_id: event.id
		  	}.to change(Comment,:count).by(1)
    	end

        it "fails because of short body" do
            @user = FactoryGirl.create :user
            sign_in @user
            comment_attr = {:body => "a", :event_id => 3, :commenter_id => 5}
            event = Event.last || create(:event, user_id: @user.id)
            expect {
                post :create, comment: comment_attr, event_id: event.id
            }.to change(Comment,:count).by(0)
        end

        it "fails because of missing body" do
            @user = FactoryGirl.create :user
            sign_in @user
            comment_attr = {:commenter_id => 5, :event_id => 3}
            event = Event.last || create(:event, user_id: @user.id)
            expect {
                post :create, comment: comment_attr, event_id: event.id
            }.to change(Comment,:count).by(0)
        end

        it "fails because of missing commenter_id" do
            @user = FactoryGirl.create :user
            sign_in @user
            comment_attr = {:body => "test", :event_id => 3}
            event = Event.last || create(:event, user_id: @user.id)
            expect {
                post :create, comment: comment_attr, event_id: event.id
            }.to change(Comment,:count).by(0)
        end
    end

    describe "DELETE #destroy" do 
    	it "deletes a comment" do
    		@user = FactoryGirl.create :user
    		sign_in @user
    		@event = FactoryGirl.create :event
    		comment = create(:comment)
			@event.comments << comment
			expect{delete :destroy, id: comment.id, event_id: @event}.
			to change{@event.comments.count}.by(-1)
			expect(response).to redirect_to event_path
    	end
    end
end
