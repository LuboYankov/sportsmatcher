require 'rails_helper'
require 'spec_helper'
require 'factory_girl_rails'

RSpec.describe JoinedEventsController, type: :controller do
	describe "POST #create" do
    	it "creates successfully a joined_event" do
    		@user = FactoryGirl.create :user
    		sign_in @user
    		joined_event_attr = attributes_for(:joined_event)
    		event = FactoryGirl.create(:event, :id => joined_event_attr[:event_id])
		  	user1 = User.last || create(:user)
		  	expect {
		    	post :create, joined_event: joined_event_attr, user_id: user1.id, :event_id => joined_event_attr[:event_id]
		  	}.to change(JoinedEvent,:count).by(1)
    	end

    	it "successfully flashes a message" do
    		@user = FactoryGirl.create :user
    		sign_in @user
    		joined_event_attr = attributes_for(:joined_event)
    		event = FactoryGirl.create(:event, :id => joined_event_attr[:event_id])
		  	user1 = User.last || create(:user)
	    	post :create, joined_event: joined_event_attr, user_id: user1.id, :event_id => joined_event_attr[:event_id]
	    	expect(flash[:notice]).not_to be_nil
    	end
    end

    describe "DELETE #destroy" do
    	it "deletes successfully a joined_event" do
    		@user = FactoryGirl.create :user
    		sign_in @user
		  	event = FactoryGirl.create(:event)
    		joined_event = FactoryGirl.create(:joined_event, :event_id => event.id, :user_id => @user.id)
		  	expect {
		    	delete :destroy, id: joined_event.id, :event_id => event.id, :user_id => @user.id 
		  	}.to change(JoinedEvent,:count).by(-1)
    	end

    	it "deletes successfully a joined_event" do
    		@user = FactoryGirl.create :user
    		sign_in @user
		  	event = FactoryGirl.create(:event)
    		joined_event = FactoryGirl.create(:joined_event, :event_id => event.id, :user_id => @user.id)
		  	delete :destroy, id: joined_event.id, :event_id => event.id, :user_id => @user.id 
		  	expect(flash[:notice]).not_to be_nil
    	end
    end
end
