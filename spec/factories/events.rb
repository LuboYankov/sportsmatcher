FactoryGirl.define do
  factory :event do
    title "title"
    description "testingdescription"
    sporttype "Football"
    location "sofia, bulgaria"
    maxplayers 12
    start_time Date.current.tomorrow
    time Time.now
  end
end