require 'rails_helper'
require 'spec_helper'
require 'factory_girl_rails'

RSpec.describe Admin, type: :model do
	it "returns events" do
		event = FactoryGirl.create(:event, title: "testaa") 
		search = FactoryGirl.create(:search)
		expect(search.search_events).not_to be_nil
	end

	it "returns nil" do 
		event = FactoryGirl.create(:event, title: "blablabla") 
		search = FactoryGirl.create(:search)
		expect(search.search_events).not_to be_nil
	end
end