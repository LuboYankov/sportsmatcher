require 'rails_helper'
require 'spec_helper'
require 'factory_girl_rails'

RSpec.describe Admin, type: :model do
	it "sets the login correctly" do 
		admin = FactoryGirl.create(:admin)
		admin.login=("test")
		expect(admin.login).to eq("test")
	end
end