require 'rails_helper'
require 'spec_helper'
require 'factory_girl_rails'

RSpec.describe Comment, type: :model do
	it "is invalid without a body" do
		expect(FactoryGirl.build(:comment, body: nil)).not_to be_valid
	end

	it "is invalid without a event_id" do
		expect(FactoryGirl.build(:comment, event_id: nil)).not_to be_valid
	end

	it "is invalid without a commenter_id" do
		expect(FactoryGirl.build(:comment, commenter_id: nil)).not_to be_valid
	end
end