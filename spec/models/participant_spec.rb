require 'rails_helper'
require 'spec_helper'
require 'factory_girl_rails'

RSpec.describe Participant, type: :model do
	it "is invalid without a user_id" do
		expect(FactoryGirl.build(:participant, user_id: nil)).not_to be_valid
	end

	it "is invalid without a event_id" do
		expect(FactoryGirl.build(:participant, event_id: nil)).not_to be_valid
	end
end