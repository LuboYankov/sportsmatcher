require 'rails_helper'
require 'spec_helper'
require 'factory_girl_rails'

RSpec.describe User, type: :model do
	it "sets the login correctly" do 
		user = FactoryGirl.create(:user)
		user.login=("test")
		expect(user.login).to eq("test")
	end

	it "doesn't participates" do 
		user = FactoryGirl.create(:user)
		event = FactoryGirl.create(:event)
		expect(user.participate?(event)).to eq(false)
	end

	it "participates" do 
		user = FactoryGirl.create(:user)
		event = FactoryGirl.create(:event)
		participant_params = {:user_id => user.id, :event_id => event.id}
		event.participants.create(participant_params)
		expect(user.participate?(event)).to eq(true)
	end

	it "is requested" do 
		user1 = FactoryGirl.create(:user, :id => 3, :email => "user1@user1.com", :username => "user1")
		user2 = FactoryGirl.create(:user, :id => 4, :email => "user2@user2.com", :username => "user2")
		
		request1 = user1.requests.build(:requested_user_id => user2.id)
	  	request2 = user2.requests.build(:requested_user_id => user1.id)

	  	expect(user1.requested?(user2)).to eq(true)
	end

	it "is requested" do 
		user1 = FactoryGirl.create(:user, :id => 3, :email => "user1@user1.com", :username => "user1")
		user2 = FactoryGirl.create(:user, :id => 4, :email => "user2@user2.com", :username => "user2")
		expect(user1.requested?(user2)).to eq(false)
	end

	it "is friend with" do 
		user1 = FactoryGirl.create(:user, :id => 3, :email => "user1@user1.com", :username => "user1")
		user2 = FactoryGirl.create(:user, :id => 4, :email => "user2@user2.com", :username => "user2")
		
		friendship1 = user1.friendships.build(:friend_id => user2.id).save!
		friendship1 = user2.friendships.build(:friend_id => user1.id).save!

		expect(user1.friend_with?(user2)).to eq(true)
	end

	it "isn't friend with" do 
		user1 = FactoryGirl.create(:user, :id => 3, :email => "user1@user1.com", :username => "user1")
		user2 = FactoryGirl.create(:user, :id => 4, :email => "user2@user2.com", :username => "user2")
		
		expect(user1.friend_with?(user2)).to eq(false)
	end
end