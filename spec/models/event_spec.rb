require 'rails_helper'
require 'spec_helper'
require 'factory_girl_rails'

RSpec.describe Event, type: :model do
	it "is invalid without a title" do
		expect(FactoryGirl.build(:event, title: nil)).not_to be_valid
	end

	it "is invalid without a description" do
		expect(FactoryGirl.build(:event, description: nil)).not_to be_valid
	end

	it "is invalid without a maxplayers" do
		expect(FactoryGirl.build(:event, maxplayers: nil)).not_to be_valid
	end

	it "is invalid without a start_time" do
		expect(FactoryGirl.build(:event, start_time: nil)).not_to be_valid
	end

	it "is invalid without a time" do
		expect(FactoryGirl.build(:event, time: nil)).not_to be_valid
	end

	it "is invalid without a sporttype" do
		expect(FactoryGirl.build(:event, sporttype: nil)).not_to be_valid
	end

	it "is invalid without a location" do
		expect(FactoryGirl.build(:event, location: nil)).not_to be_valid
	end

	it "returns the correct matches" do 
		football = FactoryGirl.create(:event, title: "football")
		foot = FactoryGirl.create(:event, title: "footba")
		baseball = FactoryGirl.create(:event, title: "baseball")
		expect(Event.search("foot")).to eq([football, foot])
	end
end
