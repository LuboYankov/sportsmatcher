class Participant < ActiveRecord::Base
	validates :user_id, presence: true
	validates :event_id, presence: true

	belongs_to :event
end
