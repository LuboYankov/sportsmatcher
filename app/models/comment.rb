class Comment < ActiveRecord::Base
  validates :body, presence: true, length: { minimum: 2, maximum: 500 }
  validates :event_id, presence: true
  validates :commenter_id, presence: true

  belongs_to :event
end
