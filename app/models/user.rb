class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  # users aren't confirmable, because the confirmation cannot be skipped when using omniauth
  devise :omniauthable, :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable, :authentication_keys => [:login]
  has_many :events
  has_many :joined_events, dependent: :destroy
  has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>", small: "20x20>" }, default_url: "/images/profile_missing.png"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/
  has_many :requests
  has_many :friendships
  has_many :friends, :through => :friendships
  
  acts_as_messageable

  attr_accessor :login

  def login=(login)
    @login = login
  end

  def login
    @login || self.username || self.email
  end

  def self.find_first_by_auth_conditions(warden_conditions)
	  conditions = warden_conditions.dup
	  if login = conditions.delete(:login)
	    where(conditions).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
	  else
	    if conditions[:username].nil?
	      where(conditions).first
	    else
	      where(username: conditions[:username]).first
	    end
	  end
	end

  def self.find_for_twitter_oauth(auth, signed_in_resource=nil)
    user = User.where(:provider => auth.provider, :uid => auth.uid).first
    if user
      return user
    else
      registered_user = User.where(:email => auth.uid + "@twitter.com").first
      if registered_user
        return registered_user
      else
        user = User.create(username:auth.extra.raw_info.name,
                            provider:auth.provider,
                            uid:auth.uid,
                            email:auth.uid+"@twitter.com",
                            password:Devise.friendly_token[0,20],
                            confirmed_at: Time.now.utc
                          )
      end
    end
  end

  def self.connect_to_linkedin(auth, signed_in_resource=nil)
    user = User.where(:provider => auth.provider, :uid => auth.uid).first
    if user
      return user
    else
      registered_user = User.where(:email => auth.info.email).first
      if registered_user
        return registered_user
      else
        user = User.create(username:auth.info.first_name,
                            provider:auth.provider,
                            uid:auth.uid,
                            email:auth.info.email,
                            password:Devise.friendly_token[0,20],
                            confirmed_at: Time.now.utc
                          )
      end
    end
  end

  def friend_with?(other_user)
    self.friends.each do |friend|
      if friend.id == other_user.id
        return true
      end
    end
    return false
  end  

  def requested?(other_user)
    other_user.requests.each do |request|
      if request.requested_user_id == self.id
        return true
      end 
    end
    return false
  end

  def participate?(event)
    event.participants.each do |participant|
      if self.id == participant.user_id
        return true
      end
    end
    return false
  end

  def mailboxer_name
    self.name
  end

  def mailboxer_email(object)
    self.email
  end

end
