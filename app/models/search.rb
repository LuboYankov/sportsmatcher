class Search < ActiveRecord::Base
	geocoded_by :location
	after_validation :geocode, :if => :location_changed?

	def search_events
		events = Event.all
		events = Event.near(location, 20) if location.present?

		events = events.where(["title LIKE ?","%#{keywords}%"]) if keywords.present?
		events = events.where(["sporttype LIKE ?","%#{sporttype}%"]) if sporttype.present?
		events = events.where(["start_time LIKE ?","%#{date}%"]) if date.present?

		return events
	end
end
