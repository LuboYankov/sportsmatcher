class Event < ActiveRecord::Base
	validates :title, presence: true, length: { minimum: 5, maximum: 20 }	
	validates :sporttype, presence: true
	validates :description, presence: true, length: { minimum: 10, maximum: 500 }
	validates :maxplayers, :numericality => { :greater_than => 1, :less_than_or_equal_to => 30 }
	validates :start_time, presence: true, timeliness: { on_or_after: lambda { Date.current }, type: :date }
	validates :time, presence: true
	validates :location, presence: true, length: { minimum: 5, maximum: 75 }

	has_attached_file :image, styles: { large: "600x600>", medium: "300x300>", thumb: "150x150#" }, :default_url => "/images/missing.png"
	validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
	has_many :participants, dependent: :destroy
	has_many :comments, dependent: :destroy
	belongs_to :user
	geocoded_by :location
	after_validation :geocode, :if => :location_changed?

	def self.search(search)
	  where("title LIKE ?", "%#{search}%")
	end
end
