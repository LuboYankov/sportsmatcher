# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready ->

  showPanel = (panelName) ->
    panels = [
      'requestsPanel'
      'eventsPanel'
      'friendsPanel'
      'aboutPanel'
      'joinedEventsPanel'
      'statisticsPanel'
    ]
    i = undefined
    i = 0
    while i < panels.length
      $('#' + panels[i]).hide()
      i++
    $('#' + panelName).show()
    return

  clickHandlers = ->
    $('#eventsButton').click ->
      showPanel 'eventsPanel'
      return
    $('#requestsButton').click ->
      showPanel 'requestsPanel'
      return
    $('#friendsButton').click ->
      showPanel 'friendsPanel'
      return
    $('#aboutButton').click ->
      showPanel 'aboutPanel'
      return
    $('#joinedEventsButton').click ->
      showPanel 'joinedEventsPanel'
      return
    $('#statisticsButton').click ->
      showPanel 'statisticsPanel'
      return
    return

  'use strict'
  clickHandlers()
  return