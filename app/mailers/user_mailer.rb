class UserMailer < ApplicationMailer
	default from: 'notifications@example.com'
 
  def commented_event(user_id, event, comment)
    @event = event
    @comment = comment
    @user = User.find(user_id)
    @commenter = User.find(@comment.commenter_id)
    mail(to: @user.email, subject: 'New comment on your event')
  end

  def joined_event(event, participant)
  	@event = event
  	@participant = participant
  	@participant_acc = User.find(@participant.user_id)
  	@user = User.find(@event.user_id)
  	mail(to: @user.email, subject: "New user joined your event")
  end

  def event_change(event, participant_acc)
    @event = event
    @participant_acc = participant_acc
    mail(to: @participant_acc.email, subject: "Changed event")
  end

  def friend_request(user1, user2)
    @user1 = user1
    @user2 = user2
    mail(to: @user1.email, subject: "New friend request")
  end

  def new_friendship(user1, user2)
    @user1 = user1
    @user2 = user2
    recipients = [@user1.email]
    recipients << @user2.email
    mail(to: recipients, subject: "New friendship")
  end

  def declined_request(user1, user2)
    @user1 = user1
    @user2 = user2
    mail(to: @user1.email, subject: "Declined friend request")
  end
end
