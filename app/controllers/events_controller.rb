class EventsController < ApplicationController
  before_action :set_event, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:index, :show]

  # GET /events
  # GET /events.json
  def index
    @events = Event.where('start_time >= ?', Date.current).paginate(:page => params[:page], :per_page => 6).order('id DESC')
    if params[:search]
      @events = Event.search(params[:search]).order("created_at DESC").paginate(:page => params[:page], :per_page => 6).order('id DESC')
    else
      @events = Event.where('start_time >= ?', Date.current).order('created_at DESC').paginate(:page => params[:page], :per_page => 6).order('id DESC')
    end
  end

  # GET /events/1
  # GET /events/1.json
  def show
    @event = Event.find(params[:id])
    @comments = @event.comments
    @comments = @comments.paginate(:page => params[:page], :per_page => 10)
    @hash = Gmaps4rails.build_markers(@event) do |event, marker|
      marker.lat event.latitude
      marker.lng event.longitude
    end
  end

  def calendar
    @events = Event.all
  end

  # GET /events/new
  def new
    @event = current_user.events.build
  end

  # GET /events/1/edit
  def edit
  end

  # POST /events
  # POST /events.json
  def create
    @event = current_user.events.build(event_params)

    respond_to do |format|
      if @event.save
        format.html { redirect_to @event, notice: 'Event was successfully created.' }
        format.json { render :show, status: :created, location: @event }
      else
        format.html { render :new }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  def notificate_users(event)
    participants = event.participants
    participants_ids = Array.new
    participants.each do |participant|
      participants_ids << participant.user_id
    end
    participants_ids.each do |participant|
      participant_acc = User.find(participant)
      if participant_acc.notifications
        UserMailer.event_change(event, participant_acc).deliver
      end
    end
  end

  def statistics
    @events = Event.all
    @football_events = Event.where("sporttype = ?", "Football")
    @basketball_events = Event.where("sporttype = ?", "Basketball")
    @baseball_events = Event.where("sporttype = ?", "Baseball")
    @volleyball_events = Event.where("sporttype = ?", "Volleyball")
    @handball_events = Event.where("sporttype = ?", "Handball")
  end

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update
    respond_to do |format|
      if @event.update(event_params)
        notificate_users(@event)
        format.html { redirect_to @event, notice: 'Event was successfully updated.' }
        format.json { render :show, status: :ok, location: @event }
      else
        format.html { render :edit }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    @event.destroy
    respond_to do |format|
      format.html { redirect_to events_url, notice: 'Event was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_params
      params.require(:event).permit(:title, :description, :maxplayers, :start_time, :time, :image, :sporttype, :location, :latitude, :longitude)
    end
end
