class JoinedEventsController < ApplicationController
	before_action :authenticate_user!

	def create
		@user = User.find(current_user.id)
		@event = Event.find(params[:event_id]) 
		@joined_event = @user.joined_events.create(joined_event_params)
		flash[:notice] = "Successfully joined the event."
		redirect_to event_path(@event)
	end

	def destroy
		@user = User.find(current_user.id)
		@event = Event.find(params[:event_id])
		@joined_event = @user.joined_events.find(params[:id])
		@joined_event.destroy
		flash[:notice] = "Successfully left the event."
		redirect_to event_path(@event)
	end

	private

	def joined_event_params
		params.require(:joined_event).permit(:user_id)
	end
end
