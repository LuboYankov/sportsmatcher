class ParticipantsController < ApplicationController
	before_action :authenticate_user!

	def create
		@event = Event.find(params[:event_id])
		@participant = @event.participants.create(participant_params)
		@joined_event = current_user.joined_events.create(:event_id => @event.id)
		if User.find(@event.user_id).notifications
			UserMailer.joined_event(@event, @participant).deliver
		end
		redirect_to event_path(@event)
	end

	def destroy
		@event = Event.find(params[:event_id])
		@participant = @event.participants.find(iterate_participants(@event))
		@joined_event = current_user.joined_events.destroy(iterate_joined_events)
		@participant.destroy
		redirect_to event_path(@event)
	end

	def iterate_joined_events
		current_user.joined_events.each do |joined_event|
			if joined_event.event_id == params[:event_id]
				return joined_event.id
			end
		end
	end

	def iterate_participants(event)
		event.participants.each do |participant|
			if participant.user_id == current_user.id
				return participant.id
			end
		end
	end

	private

	def participant_params
		params.require(:participant).permit(:user_id)
	end
end
