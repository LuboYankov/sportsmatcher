class RequestsController < ApplicationController
	def create
	  @request = User.find(params[:requested_user_id]).requests.build(:requested_user_id => current_user.id)
	  if @request.save
	  	UserMailer.friend_request(User.find(params[:requested_user_id]), current_user).deliver
	  	flash[:notice] = "Friend request sent."
	    redirect_to User.find(params[:requested_user_id])
	  else
	    flash[:notice] = "Unable to send request."
	    redirect_to root_url
	  end
	end

	def destroy
	  @request = current_user.requests.where(:requested_user_id => params[:friend_id])
	  Request.destroy(@request.first.id)
	  UserMailer.declined_request(User.find(params[:friend_id]), current_user).deliver
	  flash[:notice] = "Request declined."
	  redirect_to current_user
	end
end
