class FriendshipsController < ApplicationController
	def create
	  @friendship = current_user.friendships.build(:friend_id => params[:friend_id])
	  @inverse_friendship = User.find(params[:friend_id]).friendships.build(:friend_id => current_user.id)
	  @request = current_user.requests.where(:requested_user_id => params[:friend_id])
	  Request.destroy(@request.first.id)
	  if @friendship.save and @inverse_friendship.save
	  	UserMailer.new_friendship(User.find(params[:friend_id]), current_user).deliver
	    flash[:notice] = "Added friend."
	    redirect_to User.find(params[:friend_id])
	  else
	    flash[:notice] = "Unable to add friend."
	    redirect_to root_url
	  end
	end

	def destroy
	  @friendship = current_user.friendships.where(:friend_id => params[:id])
	  @inverse_friendship = User.find(params[:id]).friendships.where(:friend_id => current_user.id)
	  Friendship.destroy(@friendship.first.id)
	  Friendship.destroy(@inverse_friendship.first.id)
	  flash[:notice] = "Removed friendship."
	  redirect_to User.find(params[:id])
	end
end
