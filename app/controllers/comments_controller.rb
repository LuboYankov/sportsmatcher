class CommentsController < ApplicationController
	before_action :authenticate_user!

	def create
	  	@event = Event.find(params[:event_id])
	  	@comment = @event.comments.create(comment_params)
	  	if User.find(@event.user_id).notifications
	  		UserMailer.commented_event(@event.user_id, @event, @comment).deliver
	  	end
	  	redirect_to event_path(@event)
	end
 
  	def destroy
		@event = Event.find(params[:event_id])
		@comment = @event.comments.find(params[:id])
		@comment.destroy
		redirect_to event_path(@event)
	end

	private
    
	def comment_params
		params.require(:comment).permit(:body, :commenter_id)
	end
end
